package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            inputNumbers.sort(Integer::compareTo);
        } catch (OutOfMemoryError | Exception exception) {
            throw new CannotBuildPyramidException();
        }

        int row = calcRows(inputNumbers);
        int column = row * 2 - 1;

        int[][] pyramid = new int[row][column];

        int posInt = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (j + 1 >= row - i) {
                    for (int k = 0, offset = 0; k < i + 1; k++, offset += 2) {
                        pyramid[i][j + offset] = inputNumbers.get(posInt);
                        posInt++;
                    }
                    break;
                }
            }
        }
        return pyramid;
    }

    private static int calcRows(List<Integer> list) {
        for (int i = 0, sum = 0; sum <= list.size(); i++, sum = sum + i) {
            if (sum == list.size()) {
                return i;
            }
        }
        throw new CannotBuildPyramidException();
    }
}