package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        try {
            List<Lexeme> lexemes = lexemeAnalyze(statement);
            LexemeBuffer lexemeBuffer = new LexemeBuffer(lexemes);
            double db = expr(lexemeBuffer);
            if (db % 1 > 0) {
                System.out.println(db%1);
                return String.valueOf(db);
            } else {
                int in=(int)db;
                return String.valueOf(in);
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return null;
    }


    public enum LexemeType {
        LEFT_BRACKET, RIGHT_BRACKET, OP_PLUS, OP_MINUS, OP_MUL, OP_DIV, NUMBER, EOF
    }

    public static class Lexeme {
        LexemeType lexemeType;
        String value;

        public Lexeme(LexemeType lexemeType, String value) {
            this.lexemeType = lexemeType;
            this.value = value;
        }
    }

    public static List<Lexeme> lexemeAnalyze(String expText) {
        ArrayList<Lexeme> lexemes = new ArrayList<>();
        int pos = 0;
        while (pos < expText.length()) {
            char c = expText.charAt(pos);
            switch (c) {
                case '(':
                    lexemes.add(new Lexeme(LexemeType.LEFT_BRACKET, "("));
                    pos++;
                    continue;
                case ')':
                    lexemes.add(new Lexeme(LexemeType.RIGHT_BRACKET, ")"));
                    pos++;
                    continue;
                case '+':
                    lexemes.add(new Lexeme(LexemeType.OP_PLUS, "+"));
                    pos++;
                    continue;
                case '-':
                    lexemes.add(new Lexeme(LexemeType.OP_MINUS, "-"));
                    pos++;
                    continue;
                case '*':
                    lexemes.add(new Lexeme(LexemeType.OP_MUL, "*"));
                    pos++;
                    continue;
                case '/':
                    lexemes.add(new Lexeme(LexemeType.OP_DIV, "/"));
                    pos++;
                    continue;
                default:
                    if (c <= '9' && c >= '0') {
                        StringBuilder stringBuilder = new StringBuilder();
                        do {
                            stringBuilder.append(c);
                            pos++;
                            if (pos >= expText.length()) {
                                break;
                            }
                            c = expText.charAt(pos);
                        } while (c <= '9' && c >= '0' || c == '.');
                        lexemes.add(new Lexeme(LexemeType.NUMBER, stringBuilder.toString()));
                    } else {
                        if (c != ' ') {
                            throw new RuntimeException("UNEXPECTED CHARTER");
                        }
                        pos++;
                    }
            }
        }
        lexemes.add(new Lexeme(LexemeType.EOF, ""));
        return lexemes;
    }

    public static class LexemeBuffer {
        private int pos;
        public List<Lexeme> lexemes;

        public LexemeBuffer(List<Lexeme> lexemes) {
            this.lexemes = lexemes;
        }

        public Lexeme next() {
            return lexemes.get(pos++);
        }

        public void back() {
            pos--;
        }

        public int getPos() {
            return pos;
        }
    }

    public static double expr(LexemeBuffer lexemes) {
        Lexeme lexeme = lexemes.next();
        if (lexeme.lexemeType == LexemeType.EOF) {
            return 0;
        } else {
            lexemes.back();
            return plusminus(lexemes);
        }
    }

    public static double plusminus(LexemeBuffer lexemes) {
        double value = multdiv(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.lexemeType) {
                case OP_PLUS:
                    value += multdiv(lexemes);
                    break;
                case OP_MINUS:
                    value -= multdiv(lexemes);
                    break;
                case EOF:
                case RIGHT_BRACKET:
                    lexemes.back();
                    return value;
                default:
                    throw new RuntimeException("Unexpected token: " + lexeme.value
                            + " at position: " + lexemes.getPos());
            }
        }
    }

    public static double multdiv(LexemeBuffer lexemes) {
        double value = factor(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.lexemeType) {
                case OP_MUL:
                    value *= factor(lexemes);
                    break;
                case OP_DIV:
                    double temp = factor(lexemes);
                    if (temp==0){
                        throw new ArithmeticException("div 0 ex");
                    }
                    value /= temp;
                    break;
                case EOF:
                case RIGHT_BRACKET:
                case OP_PLUS:
                case OP_MINUS:
                    lexemes.back();
                    return value;
                default:
                    throw new RuntimeException("Unexpected token: " + lexeme.value
                            + " at position: " + lexemes.getPos());
            }
        }
    }

    public static double factor(LexemeBuffer lexemes) {
        Lexeme lexeme = lexemes.next();
        switch (lexeme.lexemeType) {

            case NUMBER:
                return Double.parseDouble(lexeme.value);
            case LEFT_BRACKET:
                double val = expr(lexemes);
                lexeme = lexemes.next();
                if (lexeme.lexemeType != LexemeType.RIGHT_BRACKET) {
                    throw new RuntimeException("Bad statement,: " + lexeme.value + " at pos:" + lexemes.pos);
                }
                return val;
            default:
                throw new RuntimeException("Bad statement,: " + lexeme.value + " at pos:" + lexemes.pos);
        }
    }

}
//        if (statement == null || statement.isEmpty()) {
//            return null;
//        }
//        String currentString = statement.replace(" ", "");
//        Pattern integerPattern = Pattern.compile("^\\d+");
//        Pattern doublePatter = Pattern.compile("^\\d+\\.\\d+");
//        Matcher matcher;
//        String operators = "^[+\\-*/].*";
//        double result = 1;
//        while (currentString.length() > 0) {
//            if (currentString.matches("^\\(.*")) {
//                System.out.println("(");
//                currentString = currentString.substring(1);
//            } else {
//                if (currentString.matches("^\\).*")) {
//                    currentString = currentString.substring(1);
//                    System.out.println(")");
//                } else {
//                    if (currentString.matches("^\\d.*")) {
//                        matcher = doublePatter.matcher(currentString);
//                        if (matcher.find()) {
//                            double someDouble = Double.parseDouble(currentString.substring(matcher.start(), matcher.end()));
//                        } else {
//                            matcher = integerPattern.matcher(currentString);
//                            matcher.find();
//                            int someInt = Integer.parseInt(currentString.substring(matcher.start(), matcher.end()));
//                        }
//                        System.out.println(currentString.substring(matcher.start(), matcher.end()));
//                        currentString = currentString.substring(matcher.end());
//                    } else {
//                        if (currentString.matches(operators)) {
//                            System.out.println(currentString.substring(0, 1));
//                            currentString = currentString.substring(1);
//                        }else {
//                            return null;
//                        }
//                    }
//                }
//            }
//        }
//        return String.valueOf(result);