package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() < 1) {
            return true;
        }
        if (x.size() > y.size() || y.size() < 1) {
            return false;
        }
        for (int xPos = 0, yPos = 0; yPos < y.size(); yPos++) {
            if (x.get(xPos).equals(y.get(yPos))) {
                xPos++;
                if (xPos == x.size()) {
                    return true;
                }
            }
        }
        return false;
    }
}
